Box2dSeq
=====================

Présentation
--------------------

Box2dSeq, le séquenceur à gravitation, est une application ludique permettant de composer de la musique à partir de simulation physique de chutes d’objets.

### Principe

D’une ou plusieurs sources apparaissent des objets mis en mouvement sous l’effet de la gravité. Chaque collision entraîne alors le déclenchement d’un son.

À partir de ces interactions se produisent de simples séquences rythmiques ou mélodiques qui s’enrichissent à mesure que l’instument se complexifie. De ce processus a priori contrôlé surviennent des évènements qui transforment peu à peu des séquences linéaires en compositions dynamiques voir aléatoires.

Environnement
--------------------

Box2dSeq est en cours de développement dans un environnement Ubuntu 12.04

### Openframeworks

La partie graphique est développé avec openframeworks 0.8.0. 

Les addons utilisés sont les suivants :
* ofxXmlSettings
* ofxGui
* ofxOsc
* ofxBox2d (légèrement modifié...)
* ofxLoUtils (https://gitlab.com/ofxklar/ofxLoUtils.git)


### Supercollider

La partie sonore est développé dans supercollider. Les fichiers utilisé se trouve dans le répertoire /bin/data/sc :
* oscRedirection.sc est un objet utilisé pour contrôler box2dSeq avec touchosc
* box2dSeq_control.scd déclenche les sons 
* bos2dSeq_synth.scd contient les synthés utilisé par box2dSeq_control.scd

Installation Box2dseq
====
```
mkdir Openframeworks
cd Openframeworks
wget http://www.openframeworks.cc/versions/v0.8.4/of_v0.8.4_linux64_release.tar.gz
tar -xvf of_v0.8.4_linux64_release.tar.gz
mv of_v0.8.4_linux64_release 084
cd 084/scripts/linux/ubuntu
sudo ./install_codeblocks.sh 
sudo ./install_dependencies.sh 
cd ../../../addons
git clone https://github.com/losylam/ofxBox2d.git
git clone https://gitlab.com/ofxklar/ofxLoUtils.git
cd ../apps
mkdir ofxKlar
cd ofxKlar
git clone https://gitlab.com/ofxklar/box2dseq.git
cd box2dseq
make
```
