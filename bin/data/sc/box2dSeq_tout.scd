// =====================================================================
// Entropy
// =====================================================================

s = Server.local.boot;

~of = OscRedir.new("127.0.0.1", 12346);
~sync = OscRedir.new("127.0.0.1", 6666);

thisProcess.openUDPPort(8000);
thisProcess.openUDPPort(12347);

~t = [Dictionary.newFrom(List[\vol, 1, \attack, 0.0, \release, 0.5, \modamp, 0, \modfreq, 10, \transposeoct, 0, \transpose, 0]),
	  Dictionary.newFrom(List[\vol, 1, \attack, 0.0, \release, 0.5, \modamp, 0, \modfreq, 10, \transposeoct, 0, \transpose, 0]),
	  Dictionary.newFrom(List[\vol, 1, \attack, 0.0, \release, 0.5, \modamp, 0, \modfreq, 10, \transposeoct, 0, \transpose, 0]),
	  Dictionary.newFrom(List[\vol, 1, \attack, 0.0, \release, 0.5, \modamp, 0, \modfreq, 10, \transposeoct, 0, \transpose, 0]),
	  Dictionary.newFrom(List[\vol, 1, \attack, 0.0, \release, 0.5, \modamp, 0, \modfreq, 10, \transposeoct, 0, \transpose, 0]),
];

~sync.setParam(~t);

~f_contact_osc = { arg msg, time, addr, recvPort;
	var note, vel, chan, pan, track, vol, att, rel, modw, modf;
	note = msg.at(1);
	vel = msg.at(2);
	chan = msg.at(3);
	pan = msg.at(4);
	track = msg.at(5);
	vol = 0.6;
	att = 0.04;
	rel = 0.02;
	modf = 0;
	modw = 0;
	switch(chan,
		0, {t = Synth(\loSine,
			[
				\freq, note.midicps,
				\amp, ~t[track][\vol],
				\a, ~t[track][\attack],
				\r, ~t[track][\release],
				\w, ~t[track][\modamp],
				\mod, ~t[track][\modfreq] * 50,
				\pan, pan]
		)},
		1, {t = Synth(\loSaw, [\freq, note.midicps, \amp, 0.3*vol, \a, ~attack_2 / 2, \r, ~release_2*2, \w, ~mod_amp_2, \mod, ~mod_freq_2*50, \lp_freq, ~rlpf_freq_2*1000, \rq, ~rlpf_rq_2*4]) },
		2, {t = Synth(\kickdrum, [\amp, ~t[track][\vol], \r, ~t[track][\rel]])},
		3, {t = Synth(\closedhat, [\amp, ~t[track][\vol], \r, ~t[track][\rel]])},
		4, {t = Synth(\openhat, [\amp, ~b4, \r, ~b8])},
	);
	t.release();
};
~contact_osc = OSCFunc(~f_contact_osc, '/contact');



/// OSC Relay

~sync.redirectBool('/parameters/gui');
~sync.redirectBool('/parameters/periodic_x');
~sync.redirectBool('/parameters/periodic_y');
~sync.redirectBool('/parameters/trace/draw_path');
~sync.redirectBool('/parameters/trace/width_decrease');
~sync.redirectBool('/parameters/trace/anim_contact');
~sync.redirectBool('/parameters/trace/draw_contact');
~sync.redirectBool('/parameters/trace/only_red');

~sync.redirectInt('/parameters/trace/radius_contact', 2);
~sync.redirectInt('/parameters/physic/bounce', 2);
~sync.redirectInt('/parameters/physic/gravity/x', 2);
~sync.redirectInt('/parameters/physic/gravity/y', 2);
~sync.redirectFloat('/parameters/trace/path_decrease', 3);

~sync.syncGravity();

~of.setParam(~t);
~of.syncParam('vol', true);
~of.syncParam('attack', true);
~of.syncParam('release', true);
~of.syncParam('modamp');
~of.syncParam('modfreq');

~of.syncTranspose();

~of.radioPush('/track/select', 5);
~of.radioPush('/source/oneshot', 5);
~of.radioToggle('/source/pause', 5);


MIDIClient.init;
MIDIFunc.noteOn({|vel, note| ~of.sendMsg("/addplot", note)});

// =====================================================================
// Entropy
// =====================================================================

// ==========
// osc vol control

~e_b_vel = Bus.control(s,1);
~e_b_n = Bus.control(s,1);
~e_b_amp = Bus.control(s,1);
~e_b_on =  Bus.control(s,1);

~e_b_vel.set(0);
~e_b_n.set(10);
~e_b_amp.set(0.5);
~e_b_on.set(0.5);

~er_b_vel = Bus.control(s,1);
~er_b_n = Bus.control(s,1);
~er_b_f = [Bus.control(s, 1), Bus.control(s, 1), Bus.control(s, 1), Bus.control(s, 1)];
~er_b_a = [Bus.control(s, 1), Bus.control(s, 1), Bus.control(s, 1), Bus.control(s, 1)];
~er_b_pan = Bus.control(s,1);
~er_b_amp = Bus.control(s,1);

~er_in_amp = Bus.control(s,1);


~er_in_amp_osc = OSCFunc({arg msg, time, addr, recvPort;
	~er_in_amp.set(msg.at(1));
},
	'entropy_red/in_1');

~e_b_amp_osc = OSCFunc({arg msg, time, addr, recvPort;
	~e_b_amp.set(1-(msg.at(1)/10));
}, '/parameters/trace/path_decrease');

~er_level_osc = OSCFunc({arg msg, time, addr, recvPort;
	~er_b_amp.set(msg.at(1));
},
	'entropy_red/level');

~c2_on_osc = OSCFunc({arg msg, time, addr, recvPort;
	~c2_on = msg.at(1);
}, '/parameters/trace/anim_contact');

~c22_on_osc = OSCFunc({arg msg, time, addr, recvPort;
	~c22_on = msg.at(1);
}, '/parameters/trace/draw_contact');

~e_b_on_osc = OSCFunc({arg msg, time, addr, recvPort;
	~e_b_on.set(msg.at(1));
	~e_b_on.value.postln;
}, '/parameters/trace/draw_path');

~c2_amp_osc = OSCFunc({arg msg, time, addr, recvPort;
	~c2_amp = msg.at(1);
}, '/entropy_red/in_2');


// ==========
// entropy
// (mean velocity)

~e_mix = 1;
~e_room = 0.7;

~e_vel_mul = 50;
~e_n_mul = 100;


~e_osc = OSCFunc({arg msg, time, addr, recvPort;
	var cs;
	cs = ControlSpec(0, 0.2);
	~e_b_vel.set(LinLin.kr(msg.at(1)*~e_vel_mul));
	~e_b_n.set(cs.map(msg.at(2)/~e_n_mul));
},'entropy');


~e_synth = Synth('entropy');
~e_synth.map(\freq,~e_b_vel);
~e_synth.map(\amp_n,~e_b_n);
~e_synth.map(\amp,~e_b_amp);
~e_synth.setn(\mix, ~e_mix);
~e_synth.setn(\room, ~e_room);
~e_synth.map(\on, ~e_b_on);

// ==========
// entropy_red


~er_synth = Synth(\entropy_red);
~er_synth.map(\amp, ~er_b_amp);
~er_synth.map(\f0, ~er_b_f[0]);
~er_synth.map(\f1, ~er_b_f[1]);
~er_synth.map(\f2, ~er_b_f[2]);
~er_synth.map(\f3, ~er_b_f[3]);
~er_synth.map(\a0, ~er_b_a[0]);
~er_synth.map(\a1, ~er_b_a[1]);
~er_synth.map(\a2, ~er_b_a[2]);
~er_synth.map(\a3, ~er_b_a[3]);
~er_synth.setn(\lp, 600);

~er_osc = OSCFunc({arg msg, time, addr, recvPort;
	var cs, ff;
	cs = ControlSpec(0, 0.2);
	~er_b_vel.set(msg.at(1).value);
	~er_b_n.set(msg.at(2).value);
	~er_b_pan.set(msg.at(4).value);
	ff = ((msg.at(3)+180) *4 / 360).asInt;
	~er_b_f[ff].set((msg.at(3).value+180+20)**(1.4));
	~er_b_a[ff].set((msg.at(1).value)/60);
},
	'entropy_red');


~er_in_amp.set(0.1);
~er_in_synth = Synth(\in_entropy_0);
~er_in_synth.map(\freq, ~er_b_vel);
~er_in_synth.map(\amp, ~er_in_amp);



// ==========
// contact2


~c2_r = 0.01;
~c2_a = 0.001;

~c2_amp_mul = 200;
~c2_amp = 0.3;
~c2_on = 1;
~c2_synth = \in_entropy;
~c2_osc = OSCFunc({ arg msg, time, addr, recvPort;
	var vel, pan, amp;
	vel = msg.at(1);
	pan = msg.at(2);
	amp = ~c2_amp * vel/~c2_amp_mul;
	if (~c2_on == 1){
		t = Synth(~c2_synth, [\pan, pan,\amp, amp, \a, ~c2_a, \r, ~c2_r]).release();
	};
}, '/contact2');

// =====================================================================
// Contact2
// =====================================================================

~c22_r = 0.1;
~c22_a = 0.001;
~c22_amp_mul = 200;
~c22_amp = 0.3;
~c22_on = 1;
~c22_synth = \treebf;
~f1 = 2000;
~q1 = 14;
~l1=0.888;
~f2 = 2591;
~q2 = 20.2;
~l2 = 1;
~f3 = 1742;
~q3 = 22.1;
~l3 = 0.76;

~c22_freq = 400;

~c22_osc = OSCFunc({ arg msg, time, addr, recvPort;
	var vel, pan, amp, rr;
	vel = msg.at(1);
	pan = msg.at(2);
	//vel.postln;
	amp = abs(vel)*~c22_amp/200.0;
	rr = 1 / (amp+0.1);
	//amp = ~c2_amp * vel/~c2_amp_mul;
	if (~c22_on == 1){
		t = Synth(~c22_synth, [\freq, ~c22_freq, \pan, pan,\amp, amp, \a, ~c22_a, \rosc, ~c22_r, \f1, ~f1, \q1, ~q1, \l1, ~l1,
	\f2, ~f2, \q2, ~q2, \l2, ~l2,
	\f3, ~f3, \q3, ~q3, \l3, ~l2]).release();
	};
}, '/contact2');


~bounce_osc = OSCFunc({arg msg, time, addr, recvPort;
	//~bounce_val = msg.at(1);
	~c22_freq = msg.at(1);
}, '/entropy_red/c2_freq');


~c22_amp_level = 0.1;
~c22_amp_level_osc = OSCFunc({arg msg, time, addr, recvPort;
	~c22_amp_level = msg.at(1);
	~c22_amp_level.postln;
}, '/contact2/level');

~c22_amp_osc = OSCFunc({arg msg, time, addr, recvPort;
	~c22_amp = msg.at(1)*~c22_amp_level/40;
	~c22_amp.postln;
}, '/parameters/trace/radius_contact');

~c22_r_osc = OSCFunc({arg msg, time, addr, recvPort;
	~c22_r = msg.at(1);
}, '/entropy_red/c2_r');

~l1_osc = OSCFunc({arg msg, time, addr, recvPort;
	~l1 = msg.at(1);
}, '/entropy_red/l1');

~f1_osc = OSCFunc({arg msg, time, addr, recvPort;
	~f1 = msg.at(1)*5000;
}, '/entropy_red/f1');

~q1_osc = OSCFunc({arg msg, time, addr, recvPort;
	~q1 = msg.at(1)*63 + 0.1;
}, '/entropy_red/q1');

~l2_osc = OSCFunc({arg msg, time, addr, recvPort;
	~l2 = msg.at(1);
}, '/entropy_red/l2');

~f2_osc = OSCFunc({arg msg, time, addr, recvPort;
	~f2 = msg.at(1)*5000;
}, '/entropy_red/f2');

~q2_osc = OSCFunc({arg msg, time, addr, recvPort;
	~q2 = msg.at(1)*63 + 0.1;
}, '/entropy_red/q2');

~l3_osc = OSCFunc({arg msg, time, addr, recvPort;
	~l3 = msg.at(1);
}, '/entropy_red/l3');

~f3_osc = OSCFunc({arg msg, time, addr, recvPort;
	~f3 = msg.at(1)*5000;
}, '/entropy_red/f3');

~q3_osc = OSCFunc({arg msg, time, addr, recvPort;
	~q3 = msg.at(1)*63 + 0.1;
}, '/entropy_red/q3');






