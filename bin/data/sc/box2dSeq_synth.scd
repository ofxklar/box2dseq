

SynthDef(\loSine, {|out=0, freq=440, amp=0.3, a = 0.05, r = 0.3, pan = 0, w = 0.5, chan = 3, mod = 10, lp_freq = 200, rq = 1|
	var osc1,osc2, env1, out1, mod1, oscosc, mult;
	mult = Select.kr(freq<300, [1, 64/((-0.000541219*freq*freq) + (0.3791*freq)-2.785)]);
	mod1 = w*10*LFPulse.kr(mod, 0, 0.5);
	//osc1 = {LFSaw.ar(freq*(1+(0)), 0, amp)};
	osc1 = {SinOsc.ar(freq*(1+(0)), 0, amp)};
	osc2 = {1};
	switch(chan,
		1, {osc1 = {VarSaw.ar(freq, 0, w, 1)}},
		2, {osc1 = {LFPulse.ar(freq, 0, w, 1)}},
		3, {osc1 = {Blip.ar(freq, w*100, 1,0)}}
	);
	env1 = {EnvGen.kr(Env.perc(a, r, amp), doneAction: 2)};

	//out1 = RLPF.ar(osc1*env1*osc2, lp_freq*(1+mod1), rq);
	out1 = RLPF.ar(osc1*env1*(1+mod1), lp_freq, rq);
	//out1 = MoogFF.ar(osc1*env1*osc2, lp_freq*(1+mod1), rq);


	Out.ar(out,
		Pan2.ar(out1*AmpComp.kr(freq, 100)/((NumRunningSynths.ir+3)/4), pan));
	}, variants: (alpha: [a: 0.5, r: 0.5], beta: [a: 3, r: 0.01], gamma: [a: 0.01, r: 4])
).add;

SynthDef(\loSaw, {|out=0, freq=440, amp=0.3, a = 0.05, r = 0.3, pan = 0, w = 0.5, chan = 3, mod = 10, lp_freq = 200, rq = 1|
	var osc1,osc2, env1, out1, mod1, oscosc;
	mod1 = w*10*LFPulse.kr(mod, 0, 0.5);
	osc1 = {LFSaw.ar(freq*(1+(0)), 0, amp)};
	//osc1 = {SinOsc.ar(freq*(1+(0)), 0, amp)};
	osc2 = {1};
	switch(chan,
		1, {osc1 = {VarSaw.ar(freq, 0, w, 1)}},
		2, {osc1 = {LFPulse.ar(freq, 0, w, 1)}},
		3, {osc1 = {Blip.ar(freq, w*100, 1,0)}}
	);
	osc1.postln;
	env1 = {EnvGen.kr(Env.perc(a, r, amp), doneAction: 2)};

	out1 = RLPF.ar(osc1*env1*osc2, lp_freq*(1+mod1), rq);
	//out1 = RLPF.ar(osc1*env1*(1+mod1), lp_freq, rq);
	//out1 = MoogFF.ar(osc1*env1*osc2, lp_freq*(1+mod1), rq);


	Out.ar(out,
		Pan2.ar(out1, pan));
	}, variants: (alpha: [a: 0.5, r: 0.5], beta: [a: 3, r: 0.01], gamma: [a: 0.01, r: 4])
).add;


SynthDef(\kickdrum,{
	|amp = 0.5, r = 1|
	var osc, env, output;

	osc = {SinOsc.ar(56)};
	env = {Line.ar(amp, 0.001, r, doneAction:2)};

	output = osc * env;
	Out.ar(0,
		Pan2.ar(output, 0)
	)
}).add;

SynthDef(\openhat,{
	|amp = 0.2, r = 0.4|
	var hatosc, hatenv, hatnoise, hatoutput;

	hatnoise = {LPF.ar(WhiteNoise.ar(1),4000)};

	hatosc = {HPF.ar(hatnoise,2000)};
	hatenv = {Line.ar(amp, 0.001, r, doneAction:2)};

	hatoutput = (hatosc * hatenv);

	Out.ar(0,
		Pan2.ar(hatoutput, 0)
	)
}).add;

SynthDef(\closedhat,{
	|amp = 0.5, r = 0.1|
	var hatosc, hatenv, hatnoise, hatoutput;

	hatnoise = {LPF.ar(WhiteNoise.ar(1),6000)};

	hatosc = {HPF.ar(hatnoise,2000)};
	hatenv = {Line.ar(amp, 0.001, r, doneAction:2)};

	hatoutput = (hatosc * hatenv);

	Out.ar(0,
		Pan2.ar(hatoutput, 0)
	)
}).add;

SynthDef(\closedhat2,{
	|amp = 0.2, a = 0.1, r = 0.4, pan = 0|
	var hatosc, hatenv, hatnoise, hatoutput;

	hatnoise = {LPF.ar(WhiteNoise.ar(1),MouseX.kr(100,5000))};

	hatosc = {HPF.ar(hatnoise,MouseY.kr(100,5000))};
	hatenv = {EnvGen.kr(Env.perc(a, r, amp), doneAction: 2)};

	hatoutput = (hatosc * hatenv);

	Out.ar(0,
		Pan2.ar(hatoutput, pan)
	)
}).add;