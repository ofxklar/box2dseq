OscRedir : NetAddr{
	var param;
	var sync_gravity_bool;
	var sync_accel_bool;

	*new {
		^super.new
	}

	redirectBool {
		arg name_val;
		OSCdef.new(
			key:name_val,
			func:{
				arg msg, time, addr, recvPort;
				this.sendMsg(msg[0], msg[1].asInt());
			},
			path:name_val
		);
	}

	redirectInt {
		arg name_val;
		OSCdef.new(
			key:name_val,
			func:{
				arg msg, time, addr, recvPort;
				this.sendMsg(msg[0], msg[1].asInt());
			},
			path:name_val
		);
	}

	redirectFloat {
		arg name_val;
		OSCdef.new(
			key:name_val,
			func:{
				arg msg, time, addr, recvPort;
				this.sendMsg(msg[0], msg[1].asFloat());
			},
			path:name_val
		);
	}

	radioPush {
		arg name_val, n_val;
		for (0, n_val,
			{arg i;
				OSCdef.new(
					key:name_val++i,
					func:{
						arg msg, time, addr, recvPort;
						if(msg[1].asInt() == 1){
							this.sendMsg(name_val, i-1)}
					},
					path:name_val++'/1/'++i
				);

				(name_val++'/1/'++i).postln();
			}
		)
	}

	radioToggle {
		arg name_val, n_val;
		for (0, n_val,
			{arg i;
				OSCdef.new(
					key:name_val++i,
					func:{
						arg msg, time, addr, recvPort;
						this.sendMsg(name_val, i-1, msg[1].asInt())
					},
					path:name_val++'/1/'++i
				);

				(name_val++'/1/'++i).postln();
			}
		)
	}

	setParam {
		arg param_in;
		param = param_in;
	}

	syncParam {
		arg name_val, to_of;
		for (0, 4,
			{
				arg i;
				OSCdef.new(name_val++'/'++i,
					{ arg msg, time, addr, recvPort;
						var tr;
						param[i][name_val] = msg[1];
						if (to_of == nil, {},{this.sendMsg('/track/' ++ name_val, i, msg[1])});
					}, '/track/'++ name_val ++ '/' ++ i);
			};
		)
	}

	syncTranspose {
		var transposeosc;
		transposeosc  = {
			arg i_track, n_val;
			for (0, n_val,
				{arg i;
					OSCdef.new(
						key:'tranposeoct_'++i_track++'_'++i,
						func:{
							arg msg, time, addr, recvPort;
							if(msg[1].asInt() == 1){
								param[i_track][\transposeoct] = i-4;
								this.sendMsg('/track/transposeabs',
									i_track,
									param[i_track][\transposeoct]*12 + param[i_track][\transpose]);
								(param[i_track][\transposeoct]).postln();
							}
						},
					path:'/track/transposeoct/'++i_track++'/'++i++'/1'
				);
				}
			);
		for (0, n_val,
			{arg i;
				OSCdef.new(
					key:'tranpose_'++i_track++'_'++i,
					func:{
						arg msg, time, addr, recvPort;
						if(msg[1].asInt() == 1){
							param[i_track][\transpose] = i-4;
							this.sendMsg('/track/transposeabs',
								i_track,
								param[i_track][\transposeoct]*12 + param[i_track][\transpose]);
						}
					},
					path:'/track/transpose/'++i_track++'/'++i++'/1'
				);
			}
		)

		};


		for (0, 5,
			{arg i;
				transposeosc.value(i, 7);
			}
		)
	}

	syncGravity {
		OSCFunc(
			{arg msg, time, addr, recvPort;
				sync_gravity_bool = msg.at(1);
			},
			'/sync_gravity'
		);

		OSCFunc(
			{arg msg, time, addr, recvPort;
				sync_accel_bool = msg.at(1);
			},
			'/sync_accel'
		);


		OSCdef.new(
			key:\gravity_sync,
			func:{arg msg, time, addr, recvPort;
				if (sync_gravity_bool == 1){
					this.sendMsg('/parameters/physic/gravity/x', msg[2].asInt());
					this.sendMsg('/parameters/physic/gravity/y', msg[1].asInt());
				}
			},
			path:'/gravity'
		);


		OSCdef.new(
			key:\gravity_bool,
			func:{arg msg, time, addr, recvPort;
				if (sync_accel_bool == 1){
					this.sendMsg('/parameters/physic/gravity/x', msg[2].asInt());
					this.sendMsg('/parameters/physic/gravity/y', msg[1].asInt());
				}
			},
			path:\accxyz
		);

		OSCdef.new(
			key:\gravity_init,
			func:{arg msg, time, addr, recvPort;
				this.sendMsg('/parameters/physic/gravity/x', 0);
				this.sendMsg('/parameters/physic/gravity/y', 0);
			},
			path:'/gravity/init'
		);

		OSCdef.new(
			key:\gravity_init1,
			func:{arg msg, time, addr, recvPort;
				this.sendMsg('/parameters/physic/gravity/x', 0);
				this.sendMsg('/parameters/physic/gravity/y', 10);
				this.sendMsg('/parameters/physic/bounce', 100);
			},
			path:'/gravity/init1'
		);
	}

}