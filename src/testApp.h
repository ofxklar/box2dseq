#pragma once

#include "ofMain.h"
#include "ofxBox2d.h"
#include "ofxOsc.h"
#include "ofxLoUtils.h"
#include "ofxXmlSettings.h"
#include "ofxGui.h"
#include "ofxOscParameterSync.h"

#define HOST "localhost"
#define PORT 12345
#define PORT_SC 12347
#define PORT_RECEIVER 12346


class SourceData {
 public:
  ofPoint pt;
  int beat;
  int passed_beat;
  int track;
  int active;
  bool on;
};

class CircleData {
 public:
  ofPoint old_pos;
  int id;
  bool plot;
  int id_web;
  int type;
};

class NoteData {
 public:
  int idNote;
  int channel;
  int track;
  int note;
};

class PlotData {
 public:
  int plotID;
  int plotNote;
  int idNote;
  bool bHit;
  int id;
  int channel;
  bool plot;
  ofPoint old_pos;
  int track;
  int type;
  int id_web;
};

class TrackData {
 public:
  float volume;
  float opacity;
  int chan;
  int transpose;
  int lifespan;
  int attack;
  int release;
};

class CircleTracking {
 public:
  int lifespan;
  ofPoint pos;
  ofPoint old_pos;
  float velocity;
  int r;
  int type;
};

class testApp : public ofBaseApp{

 public:
  void setup();
  void update();
  void draw();

  void keyPressed  (int key);
  void keyReleased(int key);
  void mouseMoved(int x, int y );
  void mouseDragged(int x, int y, int button);
  void mousePressed(int x, int y, int button);
  void mouseReleased(int x, int y, int button);
  void windowResized(int w, int h);
  void dragEvent(ofDragInfo dragInfo);
  void gotMessage(ofMessage msg);

  void sendAll();
  //ofxOscParameterSync sync;
  void updateOsc();

  void saveToXml();
  void loadXmlBak();
  
  void setPlot();
  void addPlot(int x, int y, int note, int radius);
  void addCircle(int x, int y, int r, int t);
  void addSource(int x, int y);

  void removeVertex();

  void gravityChanged(int & grav);
  void bounceChanged(int & bounce);

  // this is the function for contacts
  void contactStart(ofxBox2dContactArgs &e);
  void contactEnd(ofxBox2dContactArgs &e);

  ofxOscSender sender;
  ofxOscSender sender_sc;
  ofxOscReceiver oscReceiver;

  ofxOscParameterSync sync;

  ofSoundStream soundStream;

  bool b_cursor;

  // PARAMETERS GUI
  ofxPanel guigui;

  ofParameterGroup parameters;
  ofParameterGroup physic;
  ofParameterGroup trace;

  ofParameter<bool> draw_gui;
  ofParameter<bool> mode_periodic_x;
  ofParameter<bool> mode_periodic_y;
  ofParameter<bool> mode_periodic_y_down;
  ofParameterGroup gravity;
  ofParameter<int> gravity_x;
  ofParameter<int> gravity_y;

  ofParameter<float> density;
  ofParameter<int> bounce;
  ofParameter<float> friction;

  ofParameter<bool> draw_path;
  ofParameter<float> path_decrease;
  ofParameter<bool> width_decrease;
  ofParameter<int> line_width;
  ofParameter<bool> draw_contact;
  ofParameter<int> radius_contact;
  ofParameter<bool> anim_contact;
  ofParameter<bool> only_red;

  ofParameter<int> n_circles;
  ofParameter<float> entropy;
  ofParameter<float> entropy_red;
  ofParameter<float> current_fps;

  ofParameter<bool> mode_radius_note;
  ofParameter<int> plot_radius;
  ofParameter<int> note_base;
  ofParameter<int> current_channel;
  ofParameter<int> current_track;
  ofXml settings;

  // BOX2D
  ofxBox2d box2d;			  //	the box2d world
  vector <shared_ptr<ofxBox2dCircle> > circles;		  //	default box2d circles
  vector <shared_ptr<ofxBox2dCircle> > plot;		
  vector <PlotData> plot_data;
  vector <NoteData> notes;

  vector <CircleTracking> path_plot;
  ofFbo path_fbo;
  ofTexture path_fbo_tex;
  ofPixels path_fbo_pix;
  ofShader shader;
  ofMesh path_mesh;

  vector <CircleTracking> contacts;

  vector <TrackData> tracks;
  int n_tracks;

  ofxXmlSettings XML;
  int numPlotBak;

  VertexManager vertex_manager;
  VertexManager vertex_sources;
  vector <SourceData> sources;

  DelayParticles particles;

  int gamme ;
  int gammes[2][7];

  float t_trigered_ms;
  bool bang;
  float lenght_ms;

  int current_id_web;
};
