#include "testApp.h"

//--------------------------------------------------------------
void testApp::setup(){

  ofSetFrameRate(60);
  ofSetVerticalSync(true);
  ofBackground(0);
  ofEnableSmoothing();
  ofEnableAntiAliasing();
  ofSetCircleResolution(500);


  ofSetLogLevel(OF_LOG_VERBOSE);
  
  // INIT OSC
  sender.setup(HOST, PORT);
  sender_sc.setup(HOST, PORT_SC);
  ofLog(OF_LOG_NOTICE, "sending osc messages on port %i",PORT);

  // BACKUP
  if( XML.loadFile("PlotBak.xml") ){
    ofLog(OF_LOG_NOTICE, "PlotBak.xml loaded!");
    numPlotBak = XML.getNumTags("PLOT");
  }else{
    ofLog(OF_LOG_NOTICE, "unable to load PlotBak.xml check data/ folder");
  }

  b_cursor = false;
  ofHideCursor();

  // PARAMETERS // GUI
  parameters.setName("parameters");
  parameters.add(draw_gui.set("gui", true));
  parameters.add(mode_periodic_x.set("periodic_x", false));
  parameters.add(mode_periodic_y.set("periodic_y", false));
  parameters.add(mode_periodic_y_down.set("periodic_y_down", false));
  
  physic.setName("physic");
  gravity.setName("gravity");
  gravity.add(gravity_x.set("x", 0, -20, 20));
  gravity.add(gravity_y.set("y", 10, -20, 20));
  //physic.add(gravity.set("gravity", ofVec2f(0,10),ofVec2f(-20,-20), ofVec2f(20,20)));
  physic.add(gravity);
  //physic.add(density.set("density", 0.5, 0, 1000));
  density.set("density", 0.5, 0, 1000);
  physic.add(bounce.set("bounce", 100, 0, 200)); 
  //physic.add(friction.set("friction", 0.9, 0, 50));
  friction.set("friction", 0.9, 0, 50);
  parameters.add(physic);
  
  trace.setName("trace");
  trace.add(draw_path.set("draw_path", true));
  trace.add(path_decrease.set("path_decrease",2, 0, 10));
  trace.add(width_decrease.set("width_decrease", true));
  //trace.add(line_width.set("line_width", 10, 0, 60));
  line_width.set("line_width", 10, 0, 60);

  trace.add(anim_contact.set("anim_contact", true));
  trace.add(draw_contact.set("draw_contact", true));
  trace.add(radius_contact.set("radius_contact",2, 0, 50));
  trace.add(only_red.set("only_red",false));

  parameters.add(trace);

  parameters.add(note_base.set("note_base", 48, 12, 120));
  parameters.add(mode_radius_note.set("mode_radius_plot", true));
  parameters.add(plot_radius.set("plot_radius", 30,0,100));
  parameters.add(current_channel.set("current_channel", 0, 0, 5));  
  parameters.add(current_track.set("current_track", 0, 0, 5));
  
  parameters.add(n_circles.set("n_circles", 0, 0, 50));
  parameters.add(entropy.set("entropy", 0.0, 0.0, 100.0));
  parameters.add(entropy_red.set("entropy_red", 0.0, 0.0, 100.0));
  parameters.add(current_fps.set("fps", 0.0, 0.0, 100.0));

  //  parameters.add(test_rect.set("rect", 0,0,10,10));
  gravity_x.addListener(this, &testApp::gravityChanged);
  gravity_y.addListener(this, &testApp::gravityChanged);

  bounce.addListener(this, &testApp::bounceChanged);

  guigui.setup(parameters);

  sync.setup((ofParameterGroup&)guigui.getParameter(),6666,"localhost",6667);

  // SETUP BOX2D

  box2d.init();
  box2d.setGravity(gravity_x, gravity_y);
  //box2d.createGround();
  box2d.setFPS(30.0);
  box2d.registerGrabbing();
  box2d.enableEvents();

  // register the listener so that we get the events
  ofAddListener(box2d.contactStartEvents, this, &testApp::contactStart);
  ofAddListener(box2d.contactEndEvents, this, &testApp::contactEnd);


  particles.setLifespan(500);

  gamme = 0;

  gammes[0][0] = 0;
  gammes[0][1] = 2;
  gammes[0][2] = 3;
  gammes[0][3] = 5;
  gammes[0][4] = 7;
  gammes[0][5] = 8;
  gammes[0][6] = 11;
  gammes[0][7] = 12;
  
  lenght_ms = 200;
  t_trigered_ms = ofGetElapsedTimeMillis();
  
  path_fbo.allocate(ofGetScreenWidth()*2, ofGetScreenHeight()*2);
  path_fbo_pix.allocate( ofGetScreenWidth()*2, ofGetScreenHeight()*2, GL_RGBA);
  path_fbo_tex.allocate( ofGetScreenWidth()*2, ofGetScreenHeight()*2, GL_RGBA);
  shader.load( "shaderVert.c", "shaderFrag.c" );


  n_tracks = 5;
  for (int i = 0; i<n_tracks; i++){
    TrackData track;
    track.volume = 1;
    track.opacity = 1;
    track.chan = 0;
    track.transpose = 0;
    track.lifespan = 500;
    tracks.push_back(track);
  }

  current_id_web = 0;

  // fake first plot to avoid a bug
  addPlot(0,0,1,0);
  vertex_manager.desactivateVertex(0);
  plot[0].get()->destroy();  
}

void testApp::gravityChanged(int & grav){
  box2d.setGravity(gravity_x, gravity_y);
  ofLog(OF_LOG_VERBOSE, "gravity changed x = %.2f; y= %.2f", float(gravity_x), float(gravity_y));
}

void testApp::bounceChanged(int & bounce){
  for (size_t i=0; i<circles.size(); i++){
    circles[i].get()->setPhysics(density, bounce/100.0, friction);
    circles[i].get()->setBounce(bounce/100.0);
  }
}

void testApp::saveToXml(){
  //XML.setValue("NUMSCREENFRAMES", n_screen_frames, 0);
  int n_plot = vertex_manager.getLength();
  if (numPlotBak > n_plot){
    for (int i = n_plot-1; i < numPlotBak; i++){
      XML.removeTag("PLOT", i);
    } 
  }
  for (int i = 1; i<n_plot; i++){
    if  (i >= numPlotBak){
      XML.addTag("PLOT");
      numPlotBak++;
    }
    XML.pushTag("PLOT", i);
    ofPoint pos = vertex_manager.getVertexPosition(i);
    XML.setValue("POS:X", pos.x, 0);
    XML.setValue("POS:Y", pos.y, 0);
    XML.setValue("RADIUS", vertex_manager.getVertexSize(i));
    XML.setValue("NOTE", notes[i].note);
    XML.popTag();
  }
  numPlotBak = n_plot;
  XML.saveFile("PlotBak.xml");
}

void testApp::loadXmlBak(){
  numPlotBak = XML.getNumTags("PLOT");
  for (int i = 0; i < numPlotBak;i++){
    XML.pushTag("PLOT", i);
    //int note = 64;
    addPlot(XML.getValue("POS:X", 0, 0), 
	    XML.getValue("POS:Y", 0, 0), 
	    XML.getValue("NOTE", 0, 0), 
	    XML.getValue("RADIUS", 0, 0));
    XML.popTag();
  }
}

//--------------------------------------------------------------
void testApp::addPlot(int x, int y, int note, int radius){
  //ofxBox2dCircle c;
  shared_ptr<ofxBox2dCircle> c = shared_ptr<ofxBox2dCircle>(new ofxBox2dCircle);
  ofColor col;
  int rad2;
  int line_width;
  bool bContour;

  switch (current_channel){
  case 0:
    if (mode_radius_note){
      rad2 = ofMap(note, 83, 36, 5, 80);
    }else{
      rad2 = radius;
    }   
    bContour = false;
    col = noteToColorFifth(note); 
    break;
  case 1:
    if (mode_radius_note){
      rad2 = ofMap(note, 83, 36, 5, 80);
    }else{
      rad2 = radius;
    }   
    bContour = false;
    col = noteToColorFifth(note); 
    break;
  case 2:
    rad2 = 60;
    bContour = true;
    col = ofColor(100);
    line_width = 4;
    break;
  case 3:
    rad2 = 20;
    bContour = true;
    col = ofColor(255);
    line_width = 2;
    break;
  }

  c.get()->setPhysics(0, 0.5, 0.9);
  c.get()->setup(box2d.getWorld(), x, y, rad2);      
  c.get()->setData(new PlotData());

  PlotData * pd = (PlotData*)c.get()->getData();
  //pd->plotNote = note;
  pd->bHit	= false;
  pd->id = vertex_manager.getLength();
  pd->idNote = notes.size();
  pd->plot = true;
  //pd->channel = current_channel;
  //pd->track = current_track;

  NoteData new_note;
  
  new_note.note = note;
  new_note.idNote = notes.size();
  new_note.channel = current_channel;
  new_note.track = current_track;

  //  plot_data.push_back(pd);
  plot.push_back(c);
  notes.push_back(new_note);

  if (pd->id){
    ofxOscMessage m;
    
    m.setAddress("/plot/new");
    m.addIntArg(pd->id);
    m.addIntArg(x);
    m.addIntArg(y);
    m.addIntArg(rad2);
    m.addIntArg(col.getHex());
    
    sender.sendMessage(m);
  }

  DraggableVertex new_vertex(ofPoint(x, y), rad2, col);
  if (bContour){
    new_vertex.setContour(col, line_width);
  }
  new_vertex.setTrack(current_track);
  new_vertex.setOpacity(tracks[current_track].opacity);
  vertex_manager.addVertex(new_vertex); 

}

//--------------------------------------------------------------
void testApp::contactStart(ofxBox2dContactArgs &e) {
  //if(e.a != NULL && e.b != NULL) { 

      
    // if we collide with the ground we do not
    // want to play a sound. this is how you do that
    if(e.a->GetType() == b2Shape::e_circle && e.b->GetType() == b2Shape::e_circle) {
			
      PlotData * aData = (PlotData*)e.a->GetBody()->GetUserData();
      PlotData * bData = (PlotData*)e.b->GetBody()->GetUserData();
			
      ofVec2f vitesse1(e.a->GetBody()->GetLinearVelocity().x,
		      e.a->GetBody()->GetLinearVelocity().y);

      ofVec2f vitesse2(e.b->GetBody()->GetLinearVelocity().x,
		       e.b->GetBody()->GetLinearVelocity().y);
      
      float vit; //= max(vitesse1.length(), vitesse2.length());
      ofPoint pos1(e.a->GetBody()->GetPosition().x, 
		   e.a->GetBody()->GetPosition().y);
      ofPoint pos2(e.b->GetBody()->GetPosition().x,
		   e.b->GetBody()->GetPosition().y);

      ofVec2f AB = (pos2-pos1).getNormalized();
      
      int note;
      ofPoint center;
      int size ;
      int rr;
      int id;
      int id2;
      int chan;
      ofColor col;
      bool is_a_plot;
      float pan;
      int track = 0;
      int screen_width = ofGetWidth();

      int type = 0;

      int idNote;
      is_a_plot = false;
      

      //center.set((e.a->GetBody()->GetPosition().x 
      // 		  + e.b->GetBody()->GetPosition().x)*OFX_BOX2D_SCALE/2,
      // 		 (e.a->GetBody()->GetPosition().y 
      //		  + e.b->GetBody()->GetPosition().y)*OFX_BOX2D_SCALE/2);

      //      vit = vitesse1.dot(pos2-pos1) + vitesse2.dot(pos1-pos2);
      vit = vitesse1.dot(AB) - vitesse2.dot(AB);
      center = (pos1+pos2)/2.;
      size = ofMap(vit, 0, 20, 0, 7);

      if(aData->plot == true) {
	aData->bHit = true;
	//idNote = aData->idNote;
	//note = notes[aData->idNote].note;
	id = aData->idNote;
	//chan = aData->channel;
	//track = aData->track;
	is_a_plot = true;
	id2 = bData->idNote;
      }else if(bData->plot == true) {
	bData->bHit = true;
	idNote = bData->idNote;
	id = bData->idNote;
	//chan = bData->channel;
	//track = bData->track;
	is_a_plot = true;
	id2 = bData->idNote;
      }else{
	//center.set((e.a->GetBody()->GetPosition().x 
	//	    + e.b->GetBody()->GetPosition().x)*OFX_BOX2D_SCALE/2,
	//	   (e.a->GetBody()->GetPosition().y 
	//	    + e.b->GetBody()->GetPosition().y)*OFX_BOX2D_SCALE/2);
	is_a_plot = false;
      }

      if ((id == 0) and (id2 == 0)){
	is_a_plot = false;
      }

      if (is_a_plot){
	chan = notes[id].channel;
	track = notes[id].track;
	note = notes[id].note;
	if (id == 0){
	  
	  chan = notes[id2].channel;
	  track = notes[id2].track;
	  note = notes[id2].note;
	  id = id2;
	}
	center = vertex_manager.getVertexPosition(id);
	col = ofColor(vertex_manager.getVertexColor(id), tracks[track].opacity*254);
	rr = vertex_manager.getVertexSize(id);
	ofPoint line_vit(0,100);
	ofPoint accel(0,50);
	CircleParticle newParticle(center, center + ofPoint(rr + 4, 0), line_vit, size);
	newParticle.setColor(col);
	newParticle.setAccel(accel);
	newParticle.setLifespan(tracks[track].lifespan);
	particles.addCircleParticle(newParticle);
	
	pan = 2*(center.x - screen_width/2)/screen_width;
	
	ofxOscMessage m;
	m.setAddress("/contact");
	ofLog(OF_LOG_VERBOSE, "send contact");
	m.addIntArg(note + tracks[track].transpose);
	m.addFloatArg(vit);
	m.addIntArg(chan);
	m.addFloatArg(pan);
	m.addFloatArg(track);
	sender_sc.sendMessage(m);
      }else{
	pan = 2*(center.x - screen_width/2)/screen_width;
	
	ofxOscMessage m;
	m.setAddress("/contact2");
	ofLog(OF_LOG_VERBOSE, "send contact2");
	m.addFloatArg(vit);
	m.addFloatArg(pan);
	m.addFloatArg(track);
	sender_sc.sendMessage(m);
	
	ofPoint line_vit(0,vit*2);
	ofPoint accel(0,50);
	col = ofColor(250);

	if((aData->type == 1) or (bData->type == 1)){
	  type = 1;
	  col = ofColor(254,0,0);
	  }
	if(anim_contact){
	  particles.addCircleParticle(center, 
				      center, 
				      line_vit, 
				      size, 
				      col,
				      accel);
	}
	
	CircleTracking ct;
	ct.pos = center;
	ct.velocity = vit;
	ct.type = type;
	contacts.push_back(ct);
      }
    }
    //}
}

//--------------------------------------------------------------
void testApp::contactEnd(ofxBox2dContactArgs &e) {
  if(e.a != NULL && e.b != NULL) { 
		
    PlotData * aData = (PlotData*)e.a->GetBody()->GetUserData();
    PlotData * bData = (PlotData*)e.b->GetBody()->GetUserData();
		
    if(aData) {
      aData->bHit = false;
    }
		
    if(bData) {
      bData->bHit = false;
    }
  }
}


//--------------------------------------------------------------
void testApp::update(){
  float diff = ofGetElapsedTimeMillis() - t_trigered_ms;
  if ( diff > lenght_ms and not bang){
    t_trigered_ms = ofGetElapsedTimeMillis() - (int(diff) % int(lenght_ms));
    bang = true;
    //ofLogVerbose() << "bang ";
  }

  sync.update();

  if (bang){
    for (size_t i = 0; i<sources.size(); i++){
      
      if (sources[i].passed_beat < sources[i].beat-1){
	sources[i].passed_beat ++;

	//ofLogVerbose() << "beat " << sources[i].beat << " " << sources[i].passed_beat;
      }else{
	sources[i].passed_beat = 0;
	if (sources[i].on){
	  addCircle(sources[i].pt.x, sources[i].pt.y, 10, 0);
	}
      }
    }
    bang = false;
  } 

  for (size_t i = 0; i<sources.size(); i++){
    sources[i].pt = vertex_sources.getVertexPosition(i);
  }

  std::vector<shared_ptr <ofxBox2dCircle> > ::iterator iter;
  float mean_speed = 0;
  float mean_speed_red = 0;
  ofPoint mean_pos_red = ofPoint(0,0);
  int n_red = 0;
  ofVec2f mean_vel_red = ofPoint(0,0);
  float mean_dir_red = 0;

  for(iter = circles.begin(); iter != circles.end();) {
    int i = 0;
    CircleTracking c;
    CircleTracking c_plus;
    bool b_plus = false;
    PlotData * cd = (PlotData*)(iter->get())->getData();
    ofPoint pos = (iter->get())->getPosition(); 
    ofVec2f vit = (iter->get())->getVelocity();
    c.old_pos = cd->old_pos;
    cd->old_pos = (iter->get())->getPosition();
    c.pos = (iter->get())->getPosition();
    c.lifespan = 100;
    c.velocity = vit.length();
    c.type = cd->type;

    c_plus.pos = (iter->get())->getPosition();
    c_plus.lifespan = 100;
    c_plus.velocity = vit.length();
    c_plus.type = cd->type;

    if (n_circles > 0){
      mean_speed += c.velocity/n_circles;
    }
  
    if (c.type == 1){
      mean_speed_red += c.velocity;
      mean_vel_red += vit;
      mean_pos_red += pos;
      n_red ++;
    }
  
    i = cd->id_web;
    
    if ( pos.y > ofGetHeight() ){
      if (mode_periodic_y_down || mode_periodic_y){
  	(iter->get())->setPosition(pos.x, pos.y - ofGetHeight());
  	(iter->get())->setVelocity(vit);
  	c_plus.old_pos = ofPoint(c.old_pos.x, c.old_pos.y - ofGetHeight());
  	c_plus.pos = (iter->get())->getPosition();
  	cd->old_pos = (iter->get())->getPosition();
  	b_plus = true;
  	++ iter;
      }else{
  	ofLog(OF_LOG_VERBOSE, "is not alive (y)");
  	(iter->get())->destroy();
  	iter = circles.erase(iter);
  	ofxOscMessage m;
  	m.setAddress("/circle/erase");
  	m.addIntArg(i);
  	sender.sendMessage(m);
      }
    }else if ( pos.y < 0){
      if (mode_periodic_y){
  	(iter->get())->setPosition(pos.x, ofGetHeight() + pos.y);
  	(iter->get())->setVelocity(vit);
  	c_plus.old_pos = ofPoint(c.old_pos.x, c.old_pos.y + ofGetHeight());
  	c_plus.pos = (iter->get())->getPosition();
  	cd->old_pos = (iter->get())->getPosition();
  	b_plus = true;
  	++ iter;
      }else{
  	ofLog(OF_LOG_VERBOSE, "is not alive (y)");
  	(iter->get())->destroy();
  	iter = circles.erase(iter);
  	ofxOscMessage m;
  	m.setAddress("/circle/erase");
  	m.addIntArg(i);
  	sender.sendMessage(m);
      }
    }else if ( pos.x < 0){
      if (mode_periodic_x){
  	(iter->get())->setPosition(ofGetWidth() + pos.x, pos.y);
  	(iter->get())->setVelocity(vit);
  	c_plus.old_pos = ofPoint(ofGetWidth() + c.old_pos.x, c.old_pos.y);
  	c_plus.pos = (iter->get())->getPosition();
  	cd->old_pos = (iter->get())->getPosition();
  	b_plus = true;
  	++ iter;
      }else{
  	ofLog(OF_LOG_VERBOSE, "is not alive (x)");
  	(iter->get())->destroy();
  	iter = circles.erase(iter);
  	ofxOscMessage m;
  	m.setAddress("/circle/erase");
  	m.addIntArg(i);
  	sender.sendMessage(m);
      }
    }else if ( pos.x > ofGetWidth()){
      if (mode_periodic_x){
  	(iter->get())->setPosition(pos.x - ofGetWidth(), pos.y);
  	(iter->get())->setVelocity(vit);
  	c_plus.old_pos = ofPoint(c.old_pos.x - ofGetWidth(), c.old_pos.y);
  	c_plus.pos = (iter->get())->getPosition();
  	cd->old_pos = (iter->get())->getPosition();
  	b_plus = true;
  	++ iter;
      }else{
  	ofLog(OF_LOG_VERBOSE, "is not alive (x)");
  	(iter->get())->destroy();
  	iter = circles.erase(iter);
  	ofxOscMessage m;
  	m.setAddress("/circle/erase");
  	m.addIntArg(i);
  	sender.sendMessage(m);
      }
    }else{
      ofxOscMessage m;
      m.setAddress("/circle/pos");
      m.addIntArg(i);
      m.addIntArg(pos.x);
      m.addIntArg(pos.y);
      sender.sendMessage(m);
      ++ iter;
    }
    
    if (draw_path){
      if (!only_red){
  	path_plot.push_back(c);
  	if (b_plus){
  	  path_plot.push_back(c_plus);
  	}
      }else{
  	if (c.type !=0){
  	  path_plot.push_back(c);
  	  if (b_plus){
  	    path_plot.push_back(c_plus);
  	  }
  	}
      }
    }else{
      path_plot.clear();
    }

  }

  entropy = mean_speed;
  
  if (n_red > 0){
    entropy_red = mean_speed_red / n_red;
    mean_dir_red = mean_vel_red.angle(ofPoint(0,1));
    mean_pos_red = mean_pos_red/n_red;
    
    
    ofxOscMessage m_red;
    m_red.setAddress("/entropy_red");
    m_red.addFloatArg(entropy_red);
    m_red.addIntArg(n_red);
    m_red.addFloatArg(mean_dir_red);
    m_red.addFloatArg((mean_pos_red.x/ofGetWidth())*2 -1);
    sender_sc.sendMessage(m_red);  
  

  }

  ofxOscMessage m;
  m.setAddress("/entropy");
  m.addFloatArg(entropy);
  m.addIntArg(n_circles);
  sender_sc.sendMessage(m);  


  // OLD OLD
  // std::vector<CircleTracking> ::iterator iter2;
  // for(iter2 = path_plot.begin(); iter2 != path_plot.end();) {
  //   (iter2)->lifespan -= path_decrease;
  //   if ((iter2)->lifespan < 0){
  //     iter2 = path_plot.erase(iter2);
  //   }else{
  //     ++ iter2;
  //   }
  // }

  n_circles = circles.size();
  current_fps = ofGetFrameRate();

  box2d.update();
  particles.update();

  updateOsc();

  // BUG INIT OSC
  if (ofGetFrameNum() == 60){
    ofLog(OF_LOG_NOTICE, "listening for osc messages on port %i",PORT_RECEIVER);
    oscReceiver.setup(PORT_RECEIVER);
  }

  // if (draw_path){
  //   for(int i=0; i<path_plot.size(); i++) {
  //     path_mesh.addVertex(path_plot[i].old_pos);
  //     path_mesh.addVertex(path_plot[i].pos);
  //   }
  // }
  // if (draw_path){
  //path_fbo.readToPixels(path_fbo_pix);
  //path_fbo_tex.loadData(path_fbo_pix, GL_RGBA);
  
  path_fbo_tex = path_fbo.getTextureReference();
  
  //unsigned char * pix = path_fbo_pix.getPixels();
  
  //for (int i = 0; i < path_fbo_pix.getHeight() * path_fbo_pix.getWidth(); i++){
  //pix[i*4]   = max(pix[i*4] - 10, 0);
  //pix[i*4+1] = max(pix[i*4+1] - 10, 0);
  //pix[i*4+2] = max(pix[i*4+2] - 10, 0);
  //}
  
  path_fbo.begin();
  shader.begin();
  shader.setUniformTexture("texture1", path_fbo_tex, 1);
  shader.setUniform1f("path_decrease", path_decrease);	//Pass float parameter "time" to shader
  //fbo_pix.mirror(1, 1);
  path_fbo_tex.draw(0,0);
  
  shader.end();
  
  //path_fbo_tex.draw(0,0);
  // ofDisableSmoothing();

  ofScale(2);
  if (draw_path){
    
    for(size_t i=0; i<path_plot.size(); i++) {
      if (path_plot[i].old_pos != ofPoint(0,0)){
	ofPushStyle();
	if (path_plot[i].type == 0){
	  ofSetColor(255,255,255);
	}else{
	  ofSetColor(255,0,0);
	}
	int l_width;
	width_decrease ? l_width = ofMap(path_plot[i].velocity, 0, 60, line_width, 1) : l_width = line_width;
	ofSetLineWidth(l_width*4);
	ofLine(path_plot[i].pos,path_plot[i].old_pos);
	ofPopStyle();
      }
    }
  }
  path_plot.clear();

  if (draw_contact){
    for (size_t i=0; i<contacts.size();i++){
      ofPushStyle();
      ofSetColor(250);
      if (contacts[i].type == 1){
	ofSetColor(254, 0, 0);
      }
      ofFill();
      ofSetLineWidth(ofMap(contacts[i].velocity, 0, 60, 4, 10));
      ofCircle(contacts[i].pos, ofMap(contacts[i].velocity, 0, 60, 0, radius_contact));
      ofPopStyle();
    }
  }
  contacts.clear();

  path_fbo.end();
  
  ofEnableSmoothing();
  // }else{
  //   path_fbo.begin();
  //   ofSetColor(0,0,0);
  //   ofFill();
  //   ofRect(0,0,ofGetScreenWidth(), ofGetScreenHeight());
  //   path_fbo.end();
    //}
}

//--------------------------------------------------------------
void testApp::draw(){
  ofSetColor(255, 255, 255);
  
  //if (draw_path){
  path_fbo.draw(0,0, ofGetScreenWidth(), ofGetScreenHeight());
  //}
  
  for(size_t i=0; i<circles.size(); i++) {
    if (circles[i].get()->isBody()){
      //circles[i]->
      PlotData * dat = (PlotData*)circles[i].get()->getData();
      if (dat->type == 0){
	circles[i].get()->draw();
      }else{
	ofPushStyle();
	ofSetColor(255,0,0);
	circles[i].get()->draw();
	ofPopStyle();
      }
    }
  }
    
  particles.draw();
  vertex_manager.draw();
  vertex_sources.draw();

  ofPushStyle();
  if (draw_gui) {guigui.draw();}
  ofPopStyle();

  ofPushStyle();
  ofNoFill();
  ofSetLineWidth(1.5);
  ofSetColor(254, 254, 254, 0);
  ofCircle(mouseX, mouseY, plot_radius);
  ofSetColor(255, 255, 255, 255);
  // ofCircle(mouseX, mouseY, plot_radius+10);
  ofLine(ofPoint(0, mouseY), ofPoint(ofGetWidth(), mouseY));
  ofLine(ofPoint(mouseX, 0), ofPoint(mouseX, ofGetHeight()));
  ofPopStyle();

}

//--------------------------------------------------------------
void testApp::updateOsc(){
  while( oscReceiver.hasWaitingMessages() ){
    ofxOscMessage m;
    string s_osc;
    oscReceiver.getNextMessage( &m );
    s_osc = m.getAddress();
    for (int i=0; i<m.getNumArgs();i++){
      s_osc += " " + m.getArgAsString(i);
    } 
    ofLog(OF_LOG_VERBOSE, "get osc message");
    //    ofLog(OF_LOG_VERBOSE, m.getAddress());
    ofLog(OF_LOG_VERBOSE, s_osc);
    int note;
    if (m.getAddress() == "/addplot"){
      note = m.getArgAsFloat(0);
      addPlot(mouseX, mouseY, note, plot_radius);
    }else if (m.getAddress() == "/plot/new"){
      int x = m.getArgAsFloat(0);
      int y = m.getArgAsFloat(1);
      note = m.getArgAsFloat(2);
      addPlot(x, y, note, plot_radius);
    }else if (m.getAddress() == "/plotradius"){
      plot_radius = m.getArgAsFloat(0);
    }else if (m.getAddress() == "/circle"){
      int x = m.getArgAsFloat(0);
      int y = m.getArgAsFloat(1);
      int r = m.getArgAsFloat(2);
      addCircle(x, y, r, 0);	
    }else if (m.getAddress() == "/source/new"){
      int x = m.getArgAsFloat(0);
      int y = m.getArgAsFloat(1);
      addSource(x, y);
    }else if (m.getAddress() == "/track/vol"){
      int tr = m.getArgAsFloat(0);
      float op = m.getArgAsFloat(1);
      tracks[tr].opacity = op;
      for (int i = 0; i <  vertex_manager.getLength() ;i++){
	DraggableVertex vert = vertex_manager.getVertex(i);
	if (vert.getTrack() == tr){
	  vertex_manager.setVertexOpacity(i, op);
	  if ((op == 0) and (plot[i].get()->alive)){
	    plot[i].get()->destroy();
	  }else if ((op!=0) and (plot[i].get()->alive == false) and vert.isActive()){
	    plot[i].get()->setup(box2d.getWorld(), vert.getPosition().x, vert.getPosition().y, vert.getRadius());
	    plot[i].get()->setData(new PlotData());

	    PlotData * pd = (PlotData*)plot[i].get()->getData();
	    pd->idNote = i;
	    pd->id = i;
	    pd->plot = true;
	    plot[i].get()->alive = true;
	    pd->bHit	= false;

	    pd->plotNote = notes[i].note;
	    pd->channel = notes[i].channel;
	    pd->track = notes[i].track; 
	  }
	}
      }
    }else if (m.getAddress() == "/track/transpose"){
      int tr = m.getArgAsFloat(0);
      int transpose = m.getArgAsFloat(1);
      tracks[tr].transpose = tracks[tr].transpose + transpose; 
    }else if (m.getAddress() == "/track/transposeabs"){
      int tr = m.getArgAsFloat(0);
      int transpose = m.getArgAsFloat(1);
      tracks[tr].transpose = transpose; 
    }else if (m.getAddress() == "/track/attack"){
      int tr = m.getArgAsFloat(0);
      tracks[tr].attack = max(int(m.getArgAsFloat(1)*1000), 100);
      tracks[tr].lifespan = tracks[tr].release + tracks[tr].attack;
    }else if (m.getAddress() == "/track/release"){
      int tr = m.getArgAsFloat(0);
      tracks[tr].release = max(int(m.getArgAsFloat(1)*1000), 100);
      tracks[tr].lifespan = tracks[tr].release + tracks[tr].attack;
    }else if (m.getAddress() == "/track/lifespan"){
      int tr = m.getArgAsFloat(0);
      tracks[tr].lifespan = max(int(m.getArgAsFloat(1)), 200);
    }else if (m.getAddress() == "/track/select"){
      current_track = m.getArgAsFloat(0);
    }else if (m.getAddress() == "/source/oneshot"){
      int tr = m.getArgAsFloat(0);
      for (size_t i = 0; i<sources.size(); i++){
	if (sources[i].track == tr and sources[i].active == true){
	  addCircle(sources[i].pt.x, sources[i].pt.y, 10, 0);
	}
      }
    }else if (m.getAddress() == "/source/togglepause"){
      int tr = m.getArgAsFloat(0);
      for (size_t i = 0; i<sources.size(); i++){
	if (sources[i].track == tr and sources[i].active == true){
	  if (sources[i].on){
	    sources[i].on = false;
	    vertex_sources.setVertexOpacity(i, 0);
	  }else{
	    sources[i].on = true;
	    vertex_sources.setVertexOpacity(i, 1);
	  }
	}
      }
    }else if (m.getAddress() == "/source/pause"){
      int tr = m.getArgAsFloat(0);
      int p = m.getArgAsFloat(1);
      for (size_t i = 0; i<sources.size(); i++){
	if (sources[i].track == tr and sources[i].active == true){
	  if (p == 0){
	    sources[i].on = false;
	    vertex_sources.setVertexOpacity(i, 0);
	  }else{
	    sources[i].on = true;
	    vertex_sources.setVertexOpacity(i, 1);
	  }
	}
      }
    }else if (m.getAddress() == "/sendall"){
      sendAll();
    }
  }
}

void testApp::addCircle(int x, int y, int r, int t){
  shared_ptr<ofxBox2dCircle> c = shared_ptr<ofxBox2dCircle>(new ofxBox2dCircle);
  c.get()->setPhysics(density, bounce/100.0, friction);
  c.get()->setup(box2d.getWorld(), x, y, r);
  
  ofLogVerbose() << "Add circle";

  c.get()->setData(new PlotData());
  PlotData * cd = (PlotData*)c.get()->getData();
  cd->old_pos = ofPoint(x, y);
  cd->plot = false;
  cd->id = -1;
  cd->id_web = current_id_web;
  cd->type = t;
  circles.push_back(c);
  current_id_web ++;
}

void testApp::addSource(int x, int y){
  DraggableVertex new_vertex2(ofPoint(x,y), 20, ofColor(254, 254, 254, 200));
  SourceData new_source;
  
  //new_vertex2.setContour(ofColor(255), 2);
  new_vertex2.setTrack(current_track);
  //new_vertex2.setType("source");
  vertex_sources.addVertex(new_vertex2);
  new_source.pt.set(x, y);
  new_source.beat = 8;
  new_source.passed_beat = 8;
  new_source.on = true;
  new_source.active = true;
  new_source.track = current_track;
 
  ofxOscMessage m;
  m.setAddress("/source/new");
  m.addIntArg(sources.size());
  m.addIntArg(x);
  m.addIntArg(y);
  sender.sendMessage(m);

  sources.push_back(new_source);
  
  
}

void testApp::removeVertex(){
  if (vertex_manager.getLength() > 0){
    for (int i = vertex_manager.getLength()-1; i >= 0; i--){
      if (vertex_manager.isOver(i, mouseX, mouseY)){
	vertex_manager.desactivateVertex(i);
	plot[i].get()->destroy();
	ofxOscMessage m;
	m.setAddress("/plot/destroy");
	m.addIntArg(i);
	sender.sendMessage(m);
      }
    }
  }
  if (vertex_sources.getLength() > 0){
    for (int i = vertex_sources.getLength()-1; i >= 0; i--){
      if (vertex_sources.isOver(i, mouseX, mouseY)){
	vertex_sources.desactivateVertex(i);
	sources[i].on = false;
	sources[i].active = false;
	ofxOscMessage m;
	m.setAddress("/source/destroy");
	m.addIntArg(i);
	sender.sendMessage(m);
      }
    }
  }
}

void testApp::sendAll(){
  for (int i = 0 ; i<vertex_manager.getLength(); i++){
    if (plot[i].get()->alive){
      ofPoint pos = vertex_manager.getVertexPosition(i);
      ofxOscMessage m;
      
      m.setAddress("/plot/old");
      m.addIntArg(i);
      m.addIntArg(pos.x);
      m.addIntArg(pos.y);
      m.addIntArg(vertex_manager.getVertexSize(i));
      m.addIntArg(vertex_manager.getVertexColor(i).getHex());
      
      sender.sendMessage(m);
      }
  }
}


//--------------------------------------------------------------
void testApp::keyPressed(int key){

  //DraggableVertex new_vertex2(ofPoint(mouseX,mouseY), 20, ofColor(254, 254, 254, 200));
  //SourceData new_source;
  ofLogVerbose() << "key " << key;
  switch(key){
  case 'F':
    ofToggleFullscreen();
    break;
  case 'G':
    draw_gui.set(!draw_gui);
    break;
  case 'S':
    saveToXml();
    // settings.serialize(parameters);
    settings.save("settings.xml");
    break;

  case 'C':
    b_cursor = !b_cursor;
    if (b_cursor){
      ofShowCursor();
    }else{
      ofHideCursor();
    }
    break;

  case 'R':
    addCircle(mouseX, mouseY, 10, 1);
    break;

  case 13:
    addSource(mouseX, mouseY);
    break;

  case 357: // up (*2)
    for (int i=0; i<vertex_sources.getLength(); i++){
      if (vertex_sources.isOver(i, mouseX, mouseY)){
	sources[i].beat = sources[i].beat * 2;
      }
    }
    break;
    
  case 359: // down (/2)
    for (int i=0; i<vertex_sources.getLength(); i++){
      if (vertex_sources.isOver(i, mouseX, mouseY)){
       if (sources[i].beat > 1) { 
	 sources[i].beat = sources[i].beat / 2.;
       }
     }
   }
   break;

  case 356: // left (-1)
    for (int i=0; i<vertex_sources.getLength(); i++){
      if (vertex_sources.isOver(i, mouseX, mouseY)){
	sources[i].beat = sources[i].beat+1;
      }
    }
    break;

  case 358: //right (+1)
    for (int i=0; i<vertex_sources.getLength(); i++){
      if (vertex_sources.isOver(i, mouseX, mouseY)){
       if (sources[i].beat > 0) { 
	 sources[i].beat --;
       }
     }
   }
   break;
    
        
  case 'L':
    loadXmlBak();
    settings.load("settings.xml");
    // settings.deserialize(parameters);
    break;
  case 'H':
    mode_periodic_x = !mode_periodic_x;
    break;
  case 'V':
    mode_periodic_y = !mode_periodic_y;
    break;


  case 8:
    removeVertex();
    break;


  case 'o':
    addCircle(ofGetWidth()/2, 0, 10, 0);
    break;
  case 'p':
    if (vertex_sources.getLength() > 0){
      for (int i = vertex_sources.getLength()-1; i >= 0; i--){
	if (vertex_sources.isOver(i, mouseX, mouseY)){
	  sources[i].on = not sources[i].on;
	}
      }
    }    
    break;

  case '1':
    current_track = 1;
    break;
  case '2':
    current_track = 2;
    break;
  case '3':
    current_track = 3;
    break;
  case '4':
    current_track = 4;
    break;
    
  case 'a':
    addPlot(mouseX, mouseY, note_base + gammes[gamme][0], plot_radius);
    break;
  case 'z':
    addPlot(mouseX, mouseY, note_base + gammes[gamme][1], plot_radius);
    break;
  case 'e':
    addPlot(mouseX, mouseY, note_base + gammes[gamme][2], plot_radius);
    break;
  case 'r':
    addPlot(mouseX, mouseY, note_base + gammes[gamme][3], plot_radius);
    break;
  case 't':
    addPlot(mouseX, mouseY, note_base + gammes[gamme][4], plot_radius);
    break;
  case 'y':
    addPlot(mouseX, mouseY, note_base + gammes[gamme][5], plot_radius);
    break;
  case 'u':
    addPlot(mouseX, mouseY, note_base + gammes[gamme][6], plot_radius);
    break;
  case 'q':
    addPlot(mouseX, mouseY, note_base + 12 + gammes[gamme][0], plot_radius);
    break;
  case 's':
    addPlot(mouseX, mouseY, note_base + 12 + gammes[gamme][1], plot_radius);
    break;
  case 'd':
    addPlot(mouseX, mouseY, note_base + 12 + gammes[gamme][2], plot_radius);
    break;
  case 'f':
    addPlot(mouseX, mouseY, note_base + 12 + gammes[gamme][3], plot_radius);
    break;
  case 'g':
    addPlot(mouseX, mouseY, note_base + 12 + gammes[gamme][4], plot_radius);
    break;
  case 'h':
    addPlot(mouseX, mouseY, note_base + 12 + gammes[gamme][5], plot_radius);
    break;
  case 'j':
    addPlot(mouseX, mouseY, note_base + 12 + gammes[gamme][6], plot_radius);
    break;

  case 'O':
    ofPoint center;
    center.set(ofGetWidth()/2, ofGetHeight()/2);;
    for(int i=0; i<circles.size(); i++) {
      circles[i].get()->addAttractionPoint(center, 0.1);		
    }
    break;
  }
}

//--------------------------------------------------------------
void testApp::keyReleased(int key){

}

//--------------------------------------------------------------
void testApp::mouseMoved(int x, int y ){

}

//--------------------------------------------------------------
void testApp::mouseDragged(int x, int y, int button){
 for(size_t i = 0; i<plot.size();i++){
   //plot[i].get()->setPosition(vertex_manager.getVertexPosition(i));
    ofPoint pos;
    pos = vertex_manager.getVertexPosition(i);
    plot[i].get()->setPosition(pos);
    // ofxOscMessage m;
    // m.setAddress("/plot/move");
    // m.addIntArg(i);
    // m.addIntArg(pos.x);
    // m.addIntArg(pos.y);
    // sender.sendMessage(m);
    // cout << i << " " << x << " " << y << endl;


  }
}

//--------------------------------------------------------------
void testApp::mousePressed(int x, int y, int button){
  switch(button){
  case 1:
    addSource(mouseX, mouseY);
    break;
  case 2:
    removeVertex();
    break;
  }
}

//--------------------------------------------------------------
void testApp::mouseReleased(int x, int y, int button){
  for(size_t i = 0; i<plot.size();i++){
    ofPoint pos;
    pos = vertex_manager.getVertexPosition(i);
    plot[i].get()->setPosition(pos);
    //ofxOscMessage m;
    //m.setAddress("/plot/move");
    //m.addIntArg(i);
    //m.addIntArg(pos.x);
    //m.addIntArg(pos.y);
    //sender.sendMessage(m);
    //cout << i << " " << x << " " << y << endl;
  }
  
}

//--------------------------------------------------------------
void testApp::windowResized(int w, int h){

}

//--------------------------------------------------------------
void testApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void testApp::dragEvent(ofDragInfo dragInfo){ 

}
